package com.example.photoapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

class Album {
    String name;

    Album(String name) {
        this.name = name;
    }
    public String toString() {   // used by ListView
        return name;
    }
}
public class Albums extends AppCompatActivity {
    private ListView listView;
    private ArrayList<Album> albums;
    public static final int EDIT_ALBUM_CODE = 1;
    public static final int ADD_ALBUM_CODE = 2;
    public static final int DELETE_ALBUM_CODE = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            FileInputStream fis = openFileInput("albums.dat");
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(fis));
            String albumName = null;
            albums = new ArrayList<Album>();
            while ((albumName = br.readLine()) != null) {
               // String[] tokens = movieInfo.split("\\|");
                albums.add(new Album(albumName));

            }
        } catch (IOException e) {
            String[] albumsList = getResources().getStringArray(R.array.albums_array);
            albums = new ArrayList<>(albumsList.length);
            for (int i = 0; i < albumsList.length; i++) {
                //String[] tokens = moviesList[i].split("\\|");
                albums.add(new Album(albumsList[i]));
            }
        }

        listView = findViewById(R.id.albums_list);
        listView.setAdapter(new ArrayAdapter<Album>(this, R.layout.album, albums));

        // show movie for possible edit when tapped
        listView.setOnItemClickListener((p, V, pos, id) ->
                showMovie(pos));
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_add:
                addAlbum();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void addAlbum() {
        Intent intent = new Intent(this, AddEditAlbum.class);
        startActivityForResult(intent, ADD_ALBUM_CODE);
    }
    protected void onActivityResult(int requestCode,
                                    int resultCode,
                                    Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK&&resultCode !=DELETE_ALBUM_CODE&&resultCode !=ADD_ALBUM_CODE) {
            return;
        }

        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            return;
        }

        // gather all info passed back by launched activity
        String name = bundle.getString(AddEditAlbum.ALBUM_NAME);
        int index = bundle.getInt(AddEditAlbum.ALBUM_INDEX);
        System.out.println("action");
        if (resultCode == RESULT_OK) {
            Album movie = albums.get(index);
            movie.name = name;
            System.out.println("edit album");
        }else if(resultCode== DELETE_ALBUM_CODE){
            albums.remove(index);
            System.out.println("remove album");
        } else {//ADD_ALBUM_CODE
            System.out.println("add album");
            albums.add(new Album(name));
        }

        // redo the adapter to reflect change^K
        listView.setAdapter(
                new ArrayAdapter<Album>(this, R.layout.album, albums));
        writeAlbums();
    }
    private void showMovie(int pos) {
        Bundle bundle = new Bundle();
        Album movie = albums.get(pos);
        bundle.putInt(viewAlbum.ALBUM_INDEX, pos);
        bundle.putString(viewAlbum.ALBUM_NAME, movie.name);
        Intent intent = new Intent(this, viewAlbum.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, EDIT_ALBUM_CODE);
    }
    private void writeAlbums(){
        try {
            FileWriter myWriter = new FileWriter("albums.dat");
            //myWriter.write("Files in Java might be tricky, but it is fun enough!");
            for(Album a:albums){
                myWriter.write(a.name+"\n");
            }
            myWriter.close();
            //System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}